#define WIFI_SSID   "UNE_HFC_75B0"
#define WIFI_PSSWD  "ACA8CEBE"

#define WIFI_TAG    "WiFi"

#define DBG_MSG(...)    printf(__VA_ARGS__)

/*----------------------------------------------
            WiFi includes
----------------------------------------------*/
#include <stdbool.h>
#include "esp_wifi.h"
#include "tcpip_adapter.h"
#include "freertos/event_groups.h"
#include "esp_err.h"
/*----------------------------------------------
            http server includes
----------------------------------------------*/
#include "esp_http_server.h"
#include <sys/param.h>
/*----------------------------------------------
            gpio includes
----------------------------------------------*/
#include "driver/gpio.h"
/*----------------------------------------------
            Common includes
----------------------------------------------*/
#include <stdint.h>
#include <stdio.h>
#include "nvs_flash.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
/*----------------------------------------------
            Global variables
----------------------------------------------*/
EventGroupHandle_t wifi_event_group;
gpio_config_t config_pin;
bool status = 0;
/*----------------------------------------------
            gpio functions
----------------------------------------------*/
void gpio_init_output ( gpio_config_t *gpio, gpio_num_t pin ) {
    gpio->mode = GPIO_MODE_OUTPUT;
    gpio->pin_bit_mask = ( 1UL<<pin );
    gpio_config(gpio);
}

bool gpio_toggle_pin ( gpio_num_t pin, bool status ) {
    status ^= 1;
    gpio_set_level(pin, status);
    return status;
}
/*----------------------------------------------
            WiFi functions
----------------------------------------------*/

enum wifi_event {
    EVENT_CONNECTED = BIT0,
};

static esp_err_t wifi_event_handler ( void *ctx, system_event_t *event ) {
    switch (event->event_id) {
    case SYSTEM_EVENT_STA_START:
        esp_wifi_connect();
        break;
    
    case SYSTEM_EVENT_STA_GOT_IP:
        xEventGroupSetBits(wifi_event_group, EVENT_CONNECTED);
        break;

    case SYSTEM_EVENT_STA_DISCONNECTED:
        xEventGroupClearBits(wifi_event_group, EVENT_CONNECTED);
        esp_wifi_connect();
        break;

    default:
        break;
    }

    return ESP_OK;
}

bool wifi_wait_connected ( TickType_t delay ) {
    return (xEventGroupWaitBits(wifi_event_group, EVENT_CONNECTED, pdFALSE, pdTRUE, delay)
            && EVENT_CONNECTED) == EVENT_CONNECTED;
}

void wifi_init ( void ) {
    wifi_event_group = xEventGroupCreate();

    tcpip_adapter_init();
    ESP_ERROR_CHECK(esp_event_loop_init(wifi_event_handler, NULL));

    wifi_init_config_t init_config = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&init_config));
    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));

    wifi_config_t config = {

        .sta = {

            .ssid = WIFI_SSID,
            .password = WIFI_PSSWD,
            
        }
    };

    ESP_LOGI(WIFI_TAG, "Setting WiFi configuration of %s \n", config.sta.ssid);
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &config));

    ESP_ERROR_CHECK(esp_wifi_start());

}

/*----------------------------------------------
            http server functions
----------------------------------------------*/
static esp_err_t get_led_handler ( httpd_req_t *req ) {

    char *buf;
    size_t buf_len = httpd_req_get_hdr_value_len(req,"Host") + 1; /* extra byte for 192.168...[/] */
    printf("Header length => Host: %d\n\r", buf_len);
    

    if (buf_len > 1) {
        buf = malloc(buf_len);
        printf("I'm in\n\r");
        if( httpd_req_get_hdr_value_str(req, "Host", buf, buf_len) == ESP_OK ) {
            printf("Header was found => Host: %s\n\r", buf);
        }

        free(buf);
    }


    buf_len = httpd_req_get_url_query_len(req) + 1;
    printf("URL Query length => %d\n\r", buf_len);

    if (buf_len > 1) {
        buf = malloc(buf_len);
        
        if ( httpd_req_get_url_query_str(req, buf, buf_len) == ESP_OK ) {
            printf("Found URL query => %s\n\r", buf);
            char param[14];
            /* LED 1 Query */
            if( httpd_query_key_value(buf, "led1", param, sizeof(param)) == ESP_OK ) {
               printf("Found URL query parameter => query1=%s\n\r", param); 
               if( *param == '0' ) {
                   gpio_set_level(GPIO_NUM_5, 0);
               } else if ( *param == '1' ) {
                   gpio_set_level(GPIO_NUM_5, 1);
               } else {} /* ERROR */
            }
            /* LED 2 Query */
            if( httpd_query_key_value(buf, "led2", param, sizeof(param)) == ESP_OK ) {
               printf("Found URL query parameter => query2=%s\n\r", param); 
               if( *param == '0' ) {
                   gpio_set_level(GPIO_NUM_27, 0);
               } else if ( *param == '1' ) {
                   gpio_set_level(GPIO_NUM_27, 1);
               } else {} /* ERROR */
            }

        }

    }

    //status = gpio_toggle_pin(GPIO_NUM_2, status);
    char  *resp =   /*"<head><title> Hello World </title></head>"
                    "<body>"
                    "<h1> Turn ON </h1>"
                    "<h2> Test HTTP page </h2>"
                    "</body>";*/
                    "<!DOCTYPE html>"
                    "<html>"
                    "<head>" 
                    "<meta charset=utf-8>"
                    "<title>Hello World</title>"
                    "</head>"
                    "<body>"
                    "<h2>HTTP COMMANDS</h2>"
                    "<form action='/led' method='post'>"
                    "<input type='radio' name='led1' value='1' checked> TURN ON LED 1 &emsp;" 
                    "<input type='radio' name='led1' value='0'>TURN OFF LED 1<br>"
                    "<input type='radio' name='led2' value='1' checked> TURN ON LED 2 &emsp;"
                    "<input type='radio' name='led2' value='0'>TURN OFF LED 2<br><br>"
                    "<input type='submit' value='SEND QUERY'>"
                    "</form>"
                    "</body>"
                    "</html>";
    httpd_resp_sendstr(req, resp);
    return ESP_OK;

}

static esp_err_t post_led_handler ( httpd_req_t *req ) {

    char content[100]; /* characters array */
    content [sizeof(content)-1] = '\0'; /* string */
    size_t recv_size = MIN(req->content_len, sizeof(content)-1);
    int remaining = req->content_len;
    ESP_LOGI("POST", "content len => %d", req->content_len);
    while (remaining > 0) {
        int ret = httpd_req_recv(req, content, recv_size); //Bytes read
        if (ret <= 0) { /* 0 return value indicates connection closed */
            if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
                //httpd_resp_send_408(req); //respond with an HTTP 408 (Request Timeout) error
                continue;
            }
        break;
        }
        remaining -= ret;
        content[ret] = '\0';
        DBG_MSG("content => %.*s\r\n",ret,content); /* [.][*]  */

        char param[14];

        if( httpd_query_key_value(content, "led1", param, sizeof(param)) == ESP_OK ) {
               if( *param == '0' ) {
                   gpio_set_level(GPIO_NUM_5, 0);
               } else if ( *param == '1' ) {
                   gpio_set_level(GPIO_NUM_5, 1);
               } else {} /* ERROR */
            }
            /* LED 2 Query */
        if( httpd_query_key_value(content, "led2", param, sizeof(param)) == ESP_OK ) {
            if( *param == '0' ) {
                gpio_set_level(GPIO_NUM_27, 0);
            } else if ( *param == '1' ) {
                gpio_set_level(GPIO_NUM_27, 1);
            } else {} /* ERROR */
        }

    }

    char  *resp =   "<!DOCTYPE html>"
                        "<html>"
                        "<head>" 
                        "<meta charset=utf-8>"
                        "<title>Hello World</title>"
                        "</head>"
                        "<body>"
                        "<h2>HTTP COMMANDS</h2>"
                        "<form action='/led' method='post'>"
                        "<input type='radio' name='led1' value='1' checked> TURN ON LED 1 &emsp;" 
                        "<input type='radio' name='led1' value='0'>TURN OFF LED 1<br>"
                        "<input type='radio' name='led2' value='1' checked> TURN ON LED 2 &emsp;"
                        "<input type='radio' name='led2' value='0'>TURN OFF LED 2<br><br>"
                        "<input type='submit' value='SEND QUERY'>"
                        "</form>"
                        "</body>"
                        "</html>";
    
    httpd_resp_send(req, resp, strlen(resp));

    return ESP_OK;
}

static httpd_uri_t get_led = {
    .uri = "/led",
    .method = HTTP_GET,
    .handler = get_led_handler,
    .user_ctx = NULL,
};

static httpd_uri_t post_led = {
    .uri = "/led",
    .method = HTTP_POST,
    .handler = post_led_handler,
    .user_ctx = NULL,
};

httpd_handle_t http_serv_start ( const uint32_t stack, UBaseType_t priority ) {

    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.stack_size = stack;
    config.task_priority = priority;
    config.max_open_sockets = 3; // WHY?
    config.uri_match_fn = httpd_uri_match_wildcard;

    httpd_handle_t server = NULL;
    if ( httpd_start(&server, &config) == ESP_OK ) {
        httpd_register_uri_handler(server, &get_led);
        httpd_register_uri_handler(server, &post_led);
    }

    return server;

}
/*----------------------------------------------
            Tasks functions
----------------------------------------------*/
static void blink_task ( void *arg ) {
    gpio_init_output(&config_pin, GPIO_NUM_2);
    bool status = 0;
    while (1) {
        status = gpio_toggle_pin(GPIO_NUM_2, status);
        vTaskDelay(1000/portTICK_PERIOD_MS);
    }
}
/*----------------------------------------------
            main function
----------------------------------------------*/
void app_main ( void ) {
    
    nvs_flash_init();
    wifi_init();
    http_serv_start(4096, 5);
    //xTaskCreate(blink_task, "blink task", 2048, NULL, 2, NULL);
    gpio_init_output(&config_pin, GPIO_NUM_5);
    gpio_init_output(&config_pin, GPIO_NUM_27);
    //bool status = 0;
}